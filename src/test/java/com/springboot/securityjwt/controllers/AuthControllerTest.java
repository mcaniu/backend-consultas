package com.springboot.securityjwt.controllers;

import com.configuration.security.jwt.JwtUtils;
import com.controllers.BackendController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.repository.RolRepository;
import com.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;


@WebMvcTest(BackendController.class) // se indica que clase se utilizara para el test
@AutoConfigureMockMvc(addFilters = false) // sin esto respondia 403
public class AuthControllerTest {

    // clases emuladoras de los llamados y mapeador de objeto java
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    // clases inyectadas para emulacion del llamado de la clase a testear
    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    UserRepository userRepository;

    @MockBean
    RolRepository rolRepository;

    @MockBean
    PasswordEncoder encoder;

    @MockBean
    JwtUtils jwtUtils;


    @Test
    void registrarUsuario() throws Exception {
        // SignupRequest signupRequest = new SignupRequest("pepito", "pepito@gmail.com", "mod", "123456789");

        // when(rolRepository.findByDescripcion(MODERADOR)).thenReturn(Optional.of(new Rol(MODERADOR)));

        // mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(signupRequest)))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andDo(print());
    }

    @Test
    void autentificacion() throws Exception {
//        LoginDTO loginRequest = new LoginDTO("pepito", "123456789");
//
//        when(rolRepository.findByDescripcion(MODERADOR)).thenReturn(Optional.of(new Rol(MODERADOR)));
//
//        mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(loginRequest)))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andDo(print());
    }

}

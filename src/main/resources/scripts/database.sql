-- tablas que permiten el acceso a la plataforma
CREATE TABLE IF NOT EXISTS `sexo`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS `grupo`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NOT NULL,
  `activo`      BOOLEAN

);

CREATE TABLE IF NOT EXISTS `domicilio`
(
  `id`        INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `direccion` VARCHAR(40) DEFAULT 'SIN INFORMACIÓN',
  `comuna`    VARCHAR(40) DEFAULT 'SIN INFORMACIÓN',
  `region`    VARCHAR(40) DEFAULT 'SIN INFORMACIÓN'
);

CREATE TABLE IF NOT EXISTS `rol`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NULL,
  `activo`      BOOLEAN
);

CREATE TABLE IF NOT EXISTS `persona`
(
  `id`               INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `rut`              VARCHAR(20)  NOT NULL UNIQUE,
  `nombres`          VARCHAR(100) NULL DEFAULT 'SIN INFORMACIÓN',
  `apellidos`        VARCHAR(100) NULL DEFAULT 'SIN INFORMACIÓN',
  `fecha_nacimiento` DATETIME,
  `sexo_id`          INTEGER           DEFAULT 0
    CONSTRAINT FK_sexo1 REFERENCES sexo,
  `grupo_id`         INTEGER           DEFAULT 0
    CONSTRAINT FK_grupo1 REFERENCES grupo,
  `domicilio_id`     INTEGER
    CONSTRAINT FK_domicilio1 REFERENCES domicilio
);

CREATE TABLE IF NOT EXISTS `usuario`
(
  `id`         INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username`   VARCHAR(120) NOT NULL UNIQUE,
  `password`   VARCHAR(100) NOT NULL,
  `blocked`    BOOLEAN,
  `admin`      BOOLEAN,
  `activo`     BOOLEAN,
  `persona_id` INTEGER
    CONSTRAINT FK_persona1 REFERENCES persona,
  `rol_id`     INTEGER
    CONSTRAINT FK_rol1 REFERENCES rol
);


-- tablas que estan asociadas a la tabla de acceso y que contiene el detalle de la persona

CREATE TABLE IF NOT EXISTS `origen`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NULL,
  `activo`      BOOLEAN
);

CREATE TABLE IF NOT EXISTS `tipo_log`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NULL,
  `activo`      BOOLEAN
);

CREATE TABLE IF NOT EXISTS `estado`
(
  `id`          INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(40) NULL,
  `activo`      BOOLEAN
);


CREATE TABLE IF NOT EXISTS `consulta`
(
  `id`                  INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `codigo`              VARCHAR(20)  NULL,
  `pregunta`            VARCHAR(200) NULL,
  `respuesta`           VARCHAR(200) NULL,
  `fecha_creacion`      DATETIME,
  `fecha_actualizacion` DATETIME,
  `usuario_creador_id`  INTEGER
    CONSTRAINT FK_usuario1 REFERENCES usuario,
  `usuario_asignado_id` INTEGER
    CONSTRAINT FK_usuario2 REFERENCES usuario,
  `origen_id`           INTEGER
    CONSTRAINT FK_origen1 REFERENCES origen,
  `estado_id`           INTEGER
    CONSTRAINT FK_estado1 REFERENCES estado
);

CREATE TABLE IF NOT EXISTS `log_consulta`
(
  `id`                INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `datos`             VARCHAR(40) NULL,
  `fecha_creacion`    DATETIME,
  `consulta_id`       INTEGER
    CONSTRAINT FK_consulta1 REFERENCES consulta,
  `usuario_actual_id` INTEGER
    CONSTRAINT FK_usuario3 REFERENCES usuario,
  `usuario_cambio_id` INTEGER
    CONSTRAINT FK_usuario4 REFERENCES usuario,
  `tipo_log_id`       INTEGER
    CONSTRAINT FK_tipolog1 REFERENCES tipo_log
);


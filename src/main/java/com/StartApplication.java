package com;

import com.entities.*;
import com.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.List;

import static com.enums.EEstado.*;
import static com.enums.EGrupo.SIN_ESPECIFICAR;
import static com.enums.EGrupo.*;
import static com.enums.EOrigen.*;
import static com.enums.ERol.*;
import static com.enums.ESexo.*;
import static com.enums.ETipoLog.ERROR;
import static com.enums.ETipoLog.PUBLICO;
import static java.util.Arrays.asList;

@SpringBootApplication
public class StartApplication {

    @Autowired
    PasswordEncoder encoder;

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }


    /**
     * Se ejecutan algunas inserciones en la tablas de prueba.
     */
    @Bean
    public CommandLineRunner run(RolRepository rolRepository,
                                 UserRepository userRepository,
                                 SexoRepository sexoRepository,
                                 PersonaRepository personaRepository,
                                 DomicilioRepository domicilioRepository,
                                 GrupoRepository grupoRepository,
                                 OrigenRepository origenRepository,
                                 TipoLogRepository tipoLogRepository,
                                 EstadoRepository estadoRepository,
                                 ConsultaRepository consultaRepository
    ) throws Exception {
        // creacion de prueba
        return (String[] args) -> {
            // INSERCION DE ROLES
            Rol rolUsuario = Rol.builder().descripcion(USUARIO).activo(true).build();
            Rol rolModerador = Rol.builder().descripcion(MODERADOR).activo(true).build();
            Rol rolAdministrador = Rol.builder().descripcion(ADMINISTRADOR).activo(true).build();
            rolRepository.saveAll(asList(rolUsuario, rolModerador, rolAdministrador));


            // INSERCION DE SEXO
            Sexo sexoFemenino = Sexo.builder().descripcion(FEMENINO).build();
            Sexo sexoMasculino = Sexo.builder().descripcion(MASCULINO).build();
            Sexo sexoOtro = Sexo.builder().descripcion(OTRO).build();
            sexoRepository.saveAll(asList(sexoFemenino, sexoMasculino, sexoOtro));


            // INSERCION DE GRUPOS
            Grupo grupo1 = Grupo.builder().descripcion(SIN_ESPECIFICAR).activo(true).build();
            Grupo grupo2 = Grupo.builder().descripcion(SOSTENEDORES).activo(true).build();
            Grupo grupo3 = Grupo.builder().descripcion(CIUDADANOS).activo(true).build();
            Grupo grupo4 = Grupo.builder().descripcion(FUNCIONARIO_NIVEL1).activo(true).build();
            Grupo grupo5 = Grupo.builder().descripcion(FUNCIONARIO_NIVEL2).activo(true).build();
            Grupo grupo6 = Grupo.builder().descripcion(FUNCIONARIO_NIVEL3).activo(true).build();
            Grupo grupo7 = Grupo.builder().descripcion(APODERADOS).activo(true).build();
            grupoRepository.saveAll(asList(grupo1, grupo2, grupo3, grupo4, grupo5, grupo6, grupo7));

            Domicilio domicilioUsuario = Domicilio.builder()
                    .comuna("comuna usuario")
                    .direccion("direcciono usuario")
                    .region("region usuario")
                    .build();

            Domicilio domicilioModerador = Domicilio.builder()
                    .comuna("comuna moderador")
                    .direccion("direcciono moderador")
                    .region("region moderador")
                    .build();

            Domicilio domicilioAdministrador = Domicilio.builder()
                    .comuna("comuna adminstrador")
                    .direccion("direcciono adminstrador")
                    .region("region adminstrador")
                    .build();

            // INSERCION DE PERSONA
            Persona personaUsuario = Persona.builder()
                    .rut("1").nombres("Juanito Pepe").apellidos("Rojas Quintero").fechaNacimiento(new Date())
                    .domicilio(domicilioRepository.save(domicilioUsuario))
                    .grupo(grupo1)
                    .sexo(sexoOtro)
                    .build();

            Persona personaModerador = Persona.builder()
                    .rut("2").nombres("Rodolfo Alberto").apellidos("Mella Donoso").fechaNacimiento(new Date())
                    .domicilio(domicilioRepository.save(domicilioModerador))
                    .grupo(grupo2)
                    .sexo(sexoOtro)
                    .build();

            Persona personaAdministrador = Persona.builder()
                    .rut("3").nombres("Marcela Carolina").apellidos("Jimenez Pereira").fechaNacimiento(new Date())
                    .domicilio(domicilioRepository.save(domicilioAdministrador))
                    .grupo(grupo4)
                    .sexo(sexoOtro)
                    .build();


            personaRepository.saveAll(asList(personaUsuario, personaModerador, personaAdministrador));


            // INSERCION DE USUARIO PARA AUTENTIFICACION
            Usuario usuario = Usuario.builder()
                    .rol(rolUsuario).activo(true).admin(false).blocked(false)
                    .username("user@gmail.com").persona(personaUsuario)
                    .password(encoder.encode("123")).build();


            Usuario moderador = Usuario.builder()
                    .rol(rolModerador).activo(true).admin(false).blocked(false)
                    .username("moderador@gmail.com").persona(personaModerador)
                    .password(encoder.encode("123")).build();

            Usuario administrador = Usuario.builder()
                    .rol(rolAdministrador).activo(true).admin(true).blocked(false)
                    .username("admin@gmail.com").persona(personaAdministrador)
                    .password(encoder.encode("123")).admin(true).build();


            List<Usuario> listadoUsuarios = userRepository.saveAll(asList(usuario, moderador, administrador));


            // INSERCION DE TABLAS DE CONSULTAS
            Origen origenTelefono = Origen.builder().descripcion(TELEFONO).activo(true).build();
            Origen origenEmail = Origen.builder().descripcion(EMAIL).activo(true).build();
            Origen origenPresencial = Origen.builder().descripcion(PRESENCIAL).activo(true).build();
            Origen origenFacebook = Origen.builder().descripcion(FACEBOOK).activo(true).build();
            Origen origenInstagram = Origen.builder().descripcion(INSTAGRAM).activo(true).build();

            origenRepository.saveAll(asList(origenEmail, origenFacebook, origenInstagram, origenPresencial, origenTelefono));


            TipoLog tipoLogPublico = TipoLog.builder().descripcion(PUBLICO).activo(true).build();
            TipoLog tipoLogError = TipoLog.builder().descripcion(ERROR).activo(true).build();

            tipoLogRepository.saveAll(asList(tipoLogPublico, tipoLogError));


            Estado estadoEnCurso = Estado.builder().descripcion(EN_CURSO).activo(true).build();
            Estado estadoCancelado = Estado.builder().descripcion(CANCELADO).activo(true).build();
            Estado estadoResuelto = Estado.builder().descripcion(RESUELTO).activo(true).build();
            Estado estadoEnEspera = Estado.builder().descripcion(EN_ESPERA).activo(true).build();

            estadoRepository.saveAll(asList(estadoEnCurso, estadoCancelado, estadoResuelto, estadoEnEspera));


            Consulta consulta1 = Consulta.builder().codigo("CON-AVVBAVB-00283")
                    .pregunta("Test1").respuesta("").fechaCreacion(new Date()).fechaActualizacion(new Date())
                    .usuarioAsignado(moderador).usuarioCreador(usuario).origen(origenEmail)
                    .estado(estadoCancelado)
                    .build();

            Consulta consulta2 = Consulta.builder().codigo("CON-AVVBAVB-003333")
                    .pregunta("Test2").respuesta("").fechaCreacion(new Date()).fechaActualizacion(new Date())
                    .usuarioAsignado(moderador).usuarioCreador(administrador).origen(origenTelefono)
                    .estado(estadoEnCurso)
                    .build();

            consultaRepository.saveAll(asList(consulta1, consulta2));


        };
    }

}

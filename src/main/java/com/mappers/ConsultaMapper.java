package com.mappers;

import com.dtos.ConsultaDTO;
import com.entities.Consulta;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ConsultaMapper {

    ConsultaMapper INSTANCE = Mappers.getMapper(ConsultaMapper.class);

    @Mapping(source = "id", target = "id")
    ConsultaDTO objectToDto(Consulta consulta);

    @Mapping(source = "id", target = "id")
    Consulta dtoToObject(ConsultaDTO consultaDTO);

    List<ConsultaDTO> map(List<Consulta> Consultas);

    List<Consulta> mapDtos(List<ConsultaDTO> consultaDTOS);

}

package com.mappers;

import com.dtos.GenericoDTO;
import com.entities.Sexo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SexoMapper {

    SexoMapper INSTANCE = Mappers.getMapper(SexoMapper.class);

    @Mapping(source = "id", target = "id")
    GenericoDTO objectToDto(Sexo sexo);

    @Mapping(source = "id", target = "id")
    Sexo dtoToObject(GenericoDTO sexoDTO);

    List<GenericoDTO> map(List<Sexo> origenes);

    List<Sexo> mapDtos(List<GenericoDTO> sexoDTOS);

}

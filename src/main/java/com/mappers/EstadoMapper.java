package com.mappers;

import com.dtos.GenericoDTO;
import com.entities.Estado;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface EstadoMapper {

    EstadoMapper INSTANCE = Mappers.getMapper(EstadoMapper.class);

    @Mapping(source = "id", target = "id")
    GenericoDTO objectToDto(Estado estado);

    @Mapping(source = "id", target = "id")
    Estado dtoToObject(GenericoDTO estadoDTO);

    List<GenericoDTO> map(List<Estado> estados);

    List<Estado> mapDtos(List<GenericoDTO> estadoDTO);

}

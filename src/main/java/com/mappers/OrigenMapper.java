package com.mappers;

import com.dtos.GenericoDTO;
import com.entities.Origen;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface OrigenMapper {

    OrigenMapper INSTANCE = Mappers.getMapper(OrigenMapper.class);

    @Mapping(source = "id", target = "id")
    GenericoDTO objectToDto(Origen origen);

    @Mapping(source = "id", target = "id")
    Origen dtoToObject(GenericoDTO origenDTO);

    List<GenericoDTO> map(List<Origen> origenes);

    List<Origen> mapDtos(List<GenericoDTO> origenDTO);

}

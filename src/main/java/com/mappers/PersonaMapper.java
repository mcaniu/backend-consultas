package com.mappers;

import com.dtos.PersonaDTO;
import com.entities.Persona;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PersonaMapper {

    PersonaMapper INSTANCE = Mappers.getMapper(PersonaMapper.class);

    @Mapping(source = "id", target = "id")
    PersonaDTO objectToDto(Persona consulta);

    @Mapping(source = "id", target = "id")
    Persona dtoToObject(PersonaDTO consultaDTO);

    List<PersonaDTO> map(List<Persona> Personas);

    List<Persona> mapDtos(List<PersonaDTO> personaDTOS);

}

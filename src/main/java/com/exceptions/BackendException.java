package com.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BackendException extends Exception {

    private String code;
    private String message;

    public BackendException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

}

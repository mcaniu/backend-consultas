package com.controllers;

import com.configuration.security.jwt.JwtUtils;
import com.configuration.security.services.UserDetailsImpl;
import com.dtos.*;
import com.repository.RolRepository;
import com.repository.UserRepository;
import com.services.BackendService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.utils.BackendUtil.*;


/**
 * Controlador que alberga todos los endpoints de servicios(consultas-usuarios-personas,etc)
 */
@Log4j2
@RestController
@RequestMapping(path = "/v1")
public class BackendController extends BaseController {

    @Autowired
    BackendService backendService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RolRepository rolRepository;

    @Autowired
    JwtUtils jwtUtils;


    /**
     * Servicio iniciar sesion con un usuario ya registrado con anterioridad.
     *
     * @param loginDTO Formulario con datos necesarios para inicio de sesion.
     * @return devuelve un ResponseEntity con el token correspondiente o un mensaje de error si falla.
     */
    @PostMapping("/login/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword())
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

            return ResponseEntity.ok(new JwtDTO(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles));
        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Servicio que nos permitira ingresar un nuevo usuario con sus respectivas credenciales para su posterior inicio de session.
     *
     * @param usuario Formulario con datos necesarios para guardar el usuario.
     * @return devuelve un ResponseEntity con un mensaje de exito o de error si falla.
     */
    @PostMapping("/login/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UsuarioDTO usuario, BindingResult result) {
        try {
            UserDetailsImpl userDetails = usuarioConectado();

            // eliminar parametros que permitan el usuario crearse como usuarios funcionarios o admins
            if (userDetails == null | (userDetails != null &&
                    userDetails.getAuthorities().stream().noneMatch(rol -> "ADMINISTRADOR".equals(rol.getAuthority())))) {
                System.out.println("SE VACIAN LOS DATOS YA QUE NO ES ADMIN");
                usuario.setRolId(null);
                usuario.setGrupoId(null);
            }


            // VALIDACIONES DTO
            if (result.hasErrors()) {
                List<String> listadoErrores = new ArrayList<>();
                for (FieldError error : result.getFieldErrors()) {
                    listadoErrores.add(error.getDefaultMessage());
                }
                return response(TIPO_RESPUESTA_ERROR, null, MSG_GUARDADO_ERROR, listadoErrores, HttpStatus.BAD_REQUEST);

            }

            TransaccionDTO transaccionDTO = backendService.createUsuario(usuario);

            return (transaccionDTO.getSuccess()) ? response(TIPO_RESPUESTA_SUCCESS, null, REGISTRO_EXITOSO, null, HttpStatus.OK) :
                    response(TIPO_RESPUESTA_ERROR, null, MSG_GUARDADO_ERROR, transaccionDTO.getValidaciones(), HttpStatus.BAD_REQUEST);


        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @GetMapping("/consultas")
    public ResponseEntity<?> getAll() {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            return response(TIPO_RESPUESTA_SUCCESS, backendService.getAll(userDetails.getId()), null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/consultas/buscar")
    public ResponseEntity<?> getAllByFilters(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String codigo,
            @RequestParam(required = false) String rutSolicitante,
            @RequestParam(required = false) String pregunta,
            @RequestParam(required = false) String respuesta,
            @RequestParam(required = false) Integer usuarioCreadorId,
            @RequestParam(required = false) Integer usuarioAsignadoId,
            @RequestParam(required = false) Integer origenId,
            @RequestParam(required = false) Integer estadoId,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date fechaCreacionDesde,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date fechaCreacionHasta) {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            // armar objeto filtro para el service
            FiltroDTO filtroDTO = FiltroDTO.builder()
                    .codigo(codigo)
                    .rutSolicitante(rutSolicitante)
                    .pregunta(pregunta)
                    .respuesta(respuesta)
                    .fechaCreacionDesde(fechaCreacionDesde)
                    .fechaCreacionHasta(fechaCreacionHasta)
                    .usuarioAsignadoId(usuarioAsignadoId)
                    .usuarioCreadorId(usuarioCreadorId)
                    .origenId(origenId)
                    .estadoId(estadoId).build();

            Pagina datos = backendService.getAllByFilters(filtroDTO, page, size);

            return response(TIPO_RESPUESTA_SUCCESS, datos, null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/consultas/{id}")
    public ResponseEntity<?> findById(@PathVariable Integer id) {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            return response(TIPO_RESPUESTA_SUCCESS, backendService.findById(id), null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    *
    * {
        "pregunta": "klklkll",
        "origenNombre": "EMAIL"
      }
    * NOTA:
    * - por defecto el usuario creador es el que esta conectado, es decir debe tener una cuenta antes de crear una consulta
    * - validar que el usuario no funcionario pueda crear consulta y crearse la cuenta de manera inmediata
    * */
    @PostMapping("/consultas")
    public ResponseEntity<?> create(@RequestBody ConsultaDTO consultaDTO) {
        try {
            UserDetailsImpl userDetails = usuarioConectado();
            consultaDTO.setUsuarioSolicitante(UsuarioDTO.builder().id(userDetails.getId()).build()); // solicitante?

            TransaccionDTO transaccionDTO = backendService.createConsulta(consultaDTO);

            return (transaccionDTO.getSuccess()) ? response(TIPO_RESPUESTA_SUCCESS, null, REGISTRO_EXITOSO, null, HttpStatus.OK) :
                    response(TIPO_RESPUESTA_ERROR, null, MSG_GUARDADO_ERROR, transaccionDTO.getValidaciones(), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
 *
 *  {
            "id": 1,
            "codigo": "CON-AVVBAVB-00283",
            "pregunta": "Test1",
            "respuesta": "",
            "usuarioAsignadoId": 2, // quiere decir que es una derivacion
            "estadoId": 1
        }
 * NOTA:
 * - por defecto el usuario creador es el que esta conectado, es decir debe tener una cuenta antes de crear una consulta
 * - validar que el usuario no funcionario pueda crear consulta y crearse la cuenta de manera inmediata
 * */
    @PostMapping("/consultas/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody ConsultaDTO consultaDTO) {
        try {
            UserDetailsImpl userDetails = usuarioConectado();
            // consultaDTO.setUsuarioCreadorId(userDetails.getId());

            TransaccionDTO transaccionDTO = backendService.updateConsulta(consultaDTO);

            return (transaccionDTO.getSuccess()) ? response(TIPO_RESPUESTA_SUCCESS, null, REGISTRO_EXITOSO, null, HttpStatus.OK) :
                    response(TIPO_RESPUESTA_ERROR, null, MSG_GUARDADO_ERROR, transaccionDTO.getValidaciones(), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<?> getUsuarioById(@PathVariable Integer id) {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            UsuarioDTO usuarioDTO = backendService.usuarioById(id);

            // eliminar parametros que permitan saber roles o accesos, solo para USUARIOS CON PRIVILEGIOS!
            if (userDetails == null | (userDetails != null &&
                    userDetails.getAuthorities().stream().noneMatch(rol -> "ADMINISTRADOR".equals(rol.getAuthority())))) {

                if (usuarioDTO != null) {
                    usuarioDTO.setActivo(null);
                    usuarioDTO.setAdmin(null);
                    usuarioDTO.setGrupoId(null);
                    usuarioDTO.setBlocked(null);
                    usuarioDTO.setBlocked(null);
                }
            }

            return response(TIPO_RESPUESTA_SUCCESS, usuarioDTO, null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/estados")
    public ResponseEntity<?> getAllEstadoConsulta() {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            return response(TIPO_RESPUESTA_SUCCESS, backendService.getAllEstados(), null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/tipos-consulta")
    public ResponseEntity<?> getAllTipoConsulta() {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            return response(TIPO_RESPUESTA_SUCCESS, backendService.getAllOrigen(), null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/tipos-sexo")
    public ResponseEntity<?> getAllTipoSexo() {
        try {
            // se deja aca el usuario conectado para solo mostrarles las consultas que estan asociadas a el
            UserDetailsImpl userDetails = usuarioConectado();

            return response(TIPO_RESPUESTA_SUCCESS, backendService.getAllTiposSexo(), null, null, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/usuarios/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Integer id, @Valid @RequestBody UsuarioDTO usuario, BindingResult result) {

        try {
            UserDetailsImpl userDetails = usuarioConectado();
            usuario.setId(id);

            // eliminar parametros que permitan el usuario crearse como usuarios funcionarios o admins
            if (userDetails == null | (userDetails != null &&
                    userDetails.getAuthorities().stream().noneMatch(rol -> "ADMINISTRADOR".equals(rol.getAuthority())))) {
                System.out.println("SE VACIAN LOS DATOS YA QUE NO ES ADMIN");
                usuario.setRolId(null);
                usuario.setGrupoId(null);
            }

            List<String> listadoErrores = new ArrayList<>();

            // VALIDACIONES DTO
            if (result.hasErrors()) {
                for (FieldError error : result.getFieldErrors()) {
                    if (error.getField().equals("email")) {
                        listadoErrores.add(error.getDefaultMessage());
                    }
                }

                if (listadoErrores.size() > 0) {
                    return response(TIPO_RESPUESTA_ERROR, null, null, listadoErrores, HttpStatus.BAD_REQUEST);
                }

            }

            TransaccionDTO transaccionDTO = backendService.updateUsuario(usuario);
            return (transaccionDTO.getSuccess()) ? response(TIPO_RESPUESTA_SUCCESS, null, REGISTRO_EXITOSO, null, HttpStatus.OK) :
                    response(TIPO_RESPUESTA_ERROR, null, MSG_GUARDADO_ERROR, transaccionDTO.getValidaciones(), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            log.error(e);
            return response(TIPO_RESPUESTA_ERROR, null, MSG_ERROR + e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


}

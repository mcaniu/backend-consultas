package com.controllers;


import com.configuration.security.services.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controlador base para todos los controladores que se vayan generando
 */
public abstract class BaseController {

    /**
     * Permite la extraccion del usuario a travez del token
     */
    protected UserDetailsImpl usuarioConectado() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getPrincipal() instanceof UserDetailsImpl) {
            return ((UserDetailsImpl) authentication.getPrincipal());
        } else {
            return null;
        }
    }

    /**
     * Permite la generacion de una estructura para la respuesta
     */
    protected ResponseEntity<?> response(String tipoEstado, Object datos, String msg, List<String> listadoErrores, HttpStatus httpStatus) {
        Map<String, Object> response = new HashMap<>();
        response.put("estado", tipoEstado);
        if (datos != null) {
            response.put("msg", msg);
        }

        if (datos != null) {
            response.put("datos", datos);
        }
        if (listadoErrores != null && listadoErrores.size() > 0) {
            response.put("validacion", listadoErrores);
        }


        return new ResponseEntity<>(response, httpStatus);
    }



}

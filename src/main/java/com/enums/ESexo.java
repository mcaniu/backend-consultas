package com.enums;

public enum ESexo {

    SIN_ESPECIFICAR,
    FEMENINO,
    MASCULINO,
    OTRO

}

package com.enums;

public enum EEstado {

    EN_CURSO,
    CANCELADO,
    RESUELTO,
    EN_ESPERA

}

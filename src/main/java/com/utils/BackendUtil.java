package com.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BackendUtil {
    public static String REGISTRO_EXITOSO = "Usuario registrado exitosamente!";
    public static String ROL_NO_ENCONTRADO = "Error: Rol no Encontrado";
    public static String EMAIL_EXISTENTE = "El correo esta en uso";
    public static String USUARIO_EXISTENTE = "Error: Username esta en uso";
    public static String[] URLS_PUBLICAS = {"/v1/login/**", "/v3/api-docs/**", "/swagger-ui/**", "/h2-console/**"};
    public static String TIPO_RESPUESTA_ERROR = "NOK";
    public static String TIPO_RESPUESTA_SUCCESS = "OK";
    public static String MSG_GUARDADO_SUCCESS = "Se ha guardado el registro exitosamente";
    public static String MSG_GUARDADO_ERROR = "Ha ocurrido un error al guardar";
    public static String MSG_ERROR = "Ha ocurrido un error interno: ";


    public static String generarCodigo() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyymmdd-hhmmss");

        String strDate = dateFormat.format(date);
        return "CON-".concat(strDate);
    }

}

package com.services;

import com.dtos.*;
import com.exceptions.BackendException;

import java.util.List;

public interface BackendService {

    // LECTURA
    List<ConsultaDTO> getAll(Integer idUsuarioConectado);

    Pagina getAllByFilters(FiltroDTO filtroDTO, int page, int size);

    ConsultaDTO findById(Integer id);

    UsuarioDTO usuarioById(Integer id);

    List<GenericoDTO> getAllOrigen();

    List<GenericoDTO> getAllEstados();

    List<GenericoDTO> getAllTiposSexo();

    // ESCRITURA

    TransaccionDTO updateConsulta(ConsultaDTO consulta) throws BackendException;

    TransaccionDTO createConsulta(ConsultaDTO consulta) throws BackendException;

    TransaccionDTO createUsuario(UsuarioDTO usuarioDTO) throws BackendException;

    TransaccionDTO updateUsuario(UsuarioDTO usuarioDTO) throws BackendException;


}

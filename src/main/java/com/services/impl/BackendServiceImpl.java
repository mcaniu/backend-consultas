package com.services.impl;

import com.dtos.*;
import com.entities.*;
import com.enums.EEstado;
import com.enums.EOrigen;
import com.exceptions.BackendException;
import com.mappers.ConsultaMapper;
import com.mappers.EstadoMapper;
import com.mappers.OrigenMapper;
import com.mappers.SexoMapper;
import com.repository.*;
import com.services.BackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.utils.BackendUtil.EMAIL_EXISTENTE;
import static com.utils.BackendUtil.generarCodigo;
import static java.util.Collections.singletonList;

@Service
public class BackendServiceImpl implements BackendService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ConsultaRepository consultaRepository;

    @Autowired
    private OrigenRepository origenRepository;

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private GrupoRepository grupoRepository;

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private SexoRepository sexoRepository;

    @Autowired
    private DomicilioRepository domicilioRepository;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    PasswordEncoder encoder;


    @PersistenceContext(unitName = "h2")
    @Qualifier(value = "h2EntityManagerFactory")
    private EntityManager em;


    @Override
    public List<ConsultaDTO> getAll(Integer idUsuarioConectado) {

        if (idUsuarioConectado == null || idUsuarioConectado == 0) {
            return new ArrayList<>();
        }
        StringBuilder sQuery = new StringBuilder("SELECT DISTINCT ");

        sQuery.append("c.id, ");
        sQuery.append("c.codigo, ");
        sQuery.append("c.pregunta, ");
        sQuery.append("c.respuesta, ");
        sQuery.append("c.fecha_creacion, ");
        sQuery.append("c.fecha_actualizacion, ");

        // CREADOR
        sQuery.append("c.usuario_creador_id, ");
        sQuery.append("pc.rut AS usuario_creador_rut,  ");
        sQuery.append("pc.nombres AS usuario_creador_nombres,  ");
        sQuery.append("pc.apellidos AS usuario_creador_apellidos,  ");

        // ASIGNADO
        sQuery.append("c.usuario_asignado_id, ");
        sQuery.append("pa.rut AS usuario_asignado_rut,  ");
        sQuery.append("pa.nombres AS usuario_asignado_nombres,  ");
        sQuery.append("pa.apellidos AS usuario_asignado_apellidos,  ");
        // ORIGEN
        sQuery.append("c.origen_id, ");
        sQuery.append("o.descripcion AS origen_nombre, ");

        // ESTADO
        sQuery.append("c.estado_id, ");
        sQuery.append("e.descripcion AS estado_nombre ");

        sQuery.append("FROM consulta c ");
        sQuery.append("JOIN usuario uc ON (uc.id = c.usuario_creador_id) ");
        sQuery.append("JOIN persona pc ON (pc.id = uc.persona_id) ");

        sQuery.append("JOIN usuario ua ON (ua.id = c.usuario_asignado_id) ");
        sQuery.append("JOIN persona pa ON (pa.id = ua.persona_id) ");

        sQuery.append("JOIN origen o ON(o.id = c.origen_id) ");
        sQuery.append("JOIN estado e ON(e.id = c.estado_id) ");
        sQuery.append("WHERE c.usuario_creador_id = :idUsuarioConectado OR  c.usuario_asignado_id = :idUsuarioConectado");

        Query q = em.createNativeQuery(sQuery.toString());
        q.setParameter("idUsuarioConectado", idUsuarioConectado);

        List<Object[]> result = q.getResultList();
        List<ConsultaDTO> consultasD = new ArrayList<>();

        ConsultaDTO plantillaConsulta;

        for (Object[] u : result) {
            plantillaConsulta = ConsultaDTO.builder()
                    .id(Integer.parseInt(u[0].toString()))
                    .codigo(u[1].toString())
                    .pregunta(u[2].toString())
                    .respuesta(u[3].toString())
                    .fechaCreacion((Date) u[4])
                    .fechaActualizacion((Date) u[5])
                    .usuarioSolicitante(UsuarioDTO.builder().id(Integer.parseInt(u[6].toString())).rut(u[7].toString()).nombres(u[8].toString()).apellidos(u[9].toString()).build())
                    .usuarioAsignado(UsuarioDTO.builder().id(Integer.parseInt(u[10].toString())).rut(u[11].toString()).nombres(u[12].toString()).apellidos(u[13].toString()).build())
                    .origenId(Integer.parseInt(u[14].toString())).origenNombre(u[15].toString())
                    .estadoId(Integer.parseInt(u[16].toString())).estadoNombre(u[17].toString())
                    .build();

            consultasD.add(plantillaConsulta);

        }

        return consultasD;

    }


    @Override
    public ConsultaDTO findById(Integer id) {
        Consulta consulta = consultaRepository.findById(id).orElse(new Consulta());

        ConsultaDTO consultaDTO = null;
        if ((id != null && id != 0 && consulta.getId() != null)) {
            Usuario creador = consulta.getUsuarioCreador();
            Usuario asignado = consulta.getUsuarioAsignado();
            Origen origen = consulta.getOrigen();
            Estado estado = consulta.getEstado();

            consultaDTO = ConsultaMapper.INSTANCE.objectToDto(consulta);

            // consultaDTO.setUsuarioCreadorId((creador == null) ? null : creador.getId());
            // consultaDTO.setUsuarioCreadorNombre((creador == null) ? null : creador.getFullname());


            UsuarioDTO asignadoDTO = UsuarioDTO.builder()
                    .id((asignado == null) ? null : asignado.getId())
                    .nombres((asignado == null) ? null : creador.getPersona().getNombres())
                    .build();

            consultaDTO.setUsuarioAsignado(asignadoDTO);


            consultaDTO.setOrigenId((origen == null) ? null : origen.getId());
            consultaDTO.setOrigenNombre((origen == null) ? null : origen.getDescripcion().name());

            consultaDTO.setEstadoId((estado == null) ? null : estado.getId());
            consultaDTO.setEstadoNombre((estado == null) ? null : estado.getDescripcion().name());

            Persona personaCreador = creador.getPersona();

            if (personaCreador.getId() != null) {
                Domicilio personaCreadorDomicilio = personaCreador.getDomicilio();

                UsuarioDTO solicitanteDTO = UsuarioDTO.builder()
                        .id(creador.getId())
                        .rut(personaCreador.getRut())
                        .nombres(personaCreador.getNombres())
                        .apellidos(personaCreador.getApellidos())
                        .comuna(personaCreadorDomicilio.getComuna())
                        .region(personaCreadorDomicilio.getRegion())
                        .sexoId(personaCreador.getSexo().getId())
                        .direccion(personaCreadorDomicilio.getDireccion())
                        .fechaNacimiento(personaCreador.getFechaNacimiento())
                        .build();

                consultaDTO.setUsuarioSolicitante(solicitanteDTO);
            }


        }

        return consultaDTO;

    }


    @Override
    public TransaccionDTO updateConsulta(ConsultaDTO consultaDTO) throws BackendException {
        // VALIDACIONES MANUALES
        List<String> validacion = this.validarConsulta(consultaDTO, true);

        if (validacion.size() > 0) {
            return TransaccionDTO.builder().validaciones(validacion).success(false).build();
        }

        Consulta consulta = consultaRepository.findById(consultaDTO.getId()).orElse(null);
        if (consulta != null) {
            consulta.setRespuesta(consultaDTO.getRespuesta() == null ? consulta.getRespuesta() : consultaDTO.getRespuesta());
            consulta.setPregunta(consultaDTO.getPregunta() == null ? consulta.getRespuesta() : consultaDTO.getPregunta());
            consulta.setFechaActualizacion(new Date());
            consulta.setUsuarioAsignado(usuarioRepository.findById(consultaDTO.getUsuarioAsignadoId() == null ?
                    0 : consultaDTO.getUsuarioAsignadoId()).orElse(consulta.getUsuarioAsignado()));

            consulta.setEstado(estadoRepository.findById(consultaDTO.getEstadoId() == null ?
                    0 : consultaDTO.getEstadoId()).orElse(consulta.getEstado())
            );

            consultaRepository.save(consulta);
        }

        return TransaccionDTO.builder().msg("OK").success(true).singleObject(singletonList(findById(consultaDTO.getId()))).build();


    }

    @Transactional
    @Override
    public TransaccionDTO createConsulta(ConsultaDTO consultaDTO) throws BackendException {
        // VALIDACIONES MANUALES
        List<String> validacion = this.validarConsulta(consultaDTO, false);

        if (validacion.size() > 0) {
            return TransaccionDTO.builder().validaciones(validacion).success(false).build();
        }

        Consulta consulta = ConsultaMapper.INSTANCE.dtoToObject(consultaDTO);
        consulta.setFechaCreacion(new Date());
        consulta.setCodigo(generarCodigo());
        consulta.setRespuesta("");

        // por defecto es creado con estado en espera
        consulta.setEstado(estadoRepository.findByDescripcion(EEstado.EN_ESPERA).orElse(null));

        // si entrega null dara error
        consulta.setUsuarioCreador(usuarioRepository.findById(consultaDTO.getUsuarioSolicitante().getId()).orElse(null));
        consulta.setOrigen(origenRepository.findByDescripcion(EOrigen.EMAIL).orElse(null));

        consultaRepository.save(consulta);

        return TransaccionDTO.builder().msg("OK").success(true).singleObject(singletonList(findById(consulta.getId()))).build();

    }

    public List<String> validarConsulta(ConsultaDTO consultaDTO, boolean esActualizacion) throws BackendException {
        List<String> listadoErrores = new ArrayList<>();

        if (consultaDTO.getPregunta() == null || consultaDTO.getPregunta().equals("")) {
            listadoErrores.add("Debe especificar la pregunta.");
        }
        if (consultaDTO.getOrigenId() == null) {
            listadoErrores.add("Debe especificar un origen.");
        } else {
            Origen origen = origenRepository.findById(consultaDTO.getOrigenId()).orElse(null);

            if (origen == null) {
                listadoErrores.add("Debe especificar un origen valido: EMAIL, TELEFONO, PRESENCIAL, INSTAGRAM, FACEBOOK");
            }
        }
        if (esActualizacion) {
            if (consultaDTO.getId() == null) {
                listadoErrores.add("Debe especificar el identificador de la consulta.");
            } else if (findById(consultaDTO.getId()) == null) {
                listadoErrores.add("Debe especificar una consulta valida.");
            }

            if (consultaDTO.getUsuarioAsignadoId() != null) {
                // aca validar el usuario al que va a realizar la reasignacion, ya que solo debe ser un usuario funcionario
                if (!usuarioRepository.existsById(consultaDTO.getUsuarioAsignadoId())) {
                    listadoErrores.add("Debe especificar un usuario usuarioAsignado valido.");
                } else if (grupoRepository.esGrupoValido(consultaDTO.getUsuarioAsignadoId()) < 1) {
                    listadoErrores.add("Debe especificar un usuario usuarioAsignado de tipo funcionario para asignar la consulta.");

                }

            }
            if (consultaDTO.getEstadoId() != null) {
                if (!estadoRepository.existsById(consultaDTO.getEstadoId())) {
                    // verificar que sea un estado valido
                    listadoErrores.add("Debe especificar un Estado valido.");

                }
            }

        }
        return listadoErrores;


    }

    public List<String> validarUsuario(UsuarioDTO usuarioDTO, boolean esActualizacion) {
        List<String> listadoErrores = new ArrayList<>();
        if (esActualizacion) {
            // VALIDACIONES ADICIONALES
            // CASOS:
            // - USUARIO ID NO EXISTE
            // - USUARIO EMAIL Y ID NO EXISTEN
            // - USUARIO EMAIL EXISTE
            // userRepository ES NECESARIO VALIDAR CON ESTE REPO????
            if (!userRepository.existsById(usuarioDTO.getId())) {
                listadoErrores.add("Usuario no encontrado.");
            } else {
                if (!userRepository.existsByUsernameAndId(usuarioDTO.getEmail(), usuarioDTO.getId())) {
                    if (userRepository.existsByUsername(usuarioDTO.getEmail())) {
                        listadoErrores.add("El correo ya se encuentra en uso.");
                    }
                }
            }

        } else {
            // VALIDACIONES ADICIONALES
            if (userRepository.existsByUsername(usuarioDTO.getEmail())) {
                listadoErrores.add(EMAIL_EXISTENTE);
            }
        }
        return listadoErrores;
    }

    @Transactional
    @Override
    public UsuarioDTO usuarioById(Integer id) {
        Usuario usuario = usuarioRepository.findById(id).orElse(new Usuario());

        UsuarioDTO usuarioDTO = null;
        if ((usuario.getId() != null && id != 0 && usuario.getId() != null)) {

            usuarioDTO = UsuarioDTO.builder()
                    .id(usuario.getId())
                    .blocked(usuario.getBlocked())
                    .activo(usuario.isActivo())
                    .email(usuario.getUsername()).build();

            if (usuario.getPersona() != null) {
                Persona persona = usuario.getPersona();
                usuarioDTO.setNombres(persona.getNombres());
                usuarioDTO.setApellidos(persona.getApellidos());
                usuarioDTO.setRut(persona.getRut());
                usuarioDTO.setFechaNacimiento(persona.getFechaNacimiento());


                if (persona.getSexo() != null) {
                    Sexo sexo = persona.getSexo();
                    usuarioDTO.setSexoId(sexo.getId());
                }

                if (persona.getDomicilio() != null) {
                    Domicilio domicilio = persona.getDomicilio();
                    usuarioDTO.setDireccion(domicilio.getDireccion());
                    usuarioDTO.setComuna(domicilio.getComuna());
                    usuarioDTO.setRegion(domicilio.getRegion());
                }

            }

        }

        return usuarioDTO;
    }


    @Override
    public Pagina getAllByFilters(FiltroDTO filtroDTO, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Specification<Consulta> parametrosBusqueda = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (filtroDTO.getRutSolicitante() != null) {
                // usuario creador
                Join<Consulta, Usuario> usuarioJoin = root.join("usuarioCreador");
                Join<Usuario, Persona> personaJoin = usuarioJoin.join("persona");

                predicates.add(criteriaBuilder.equal(personaJoin.get("id"), filtroDTO.getRutSolicitante()));
            }

            if (filtroDTO.getUsuarioAsignadoId() != null) {
                // usuario usuarioAsignado
                Join<Consulta, Usuario> usuarioJoin = root.join("usuarioAsignado");
                predicates.add(criteriaBuilder.equal(usuarioJoin.get("id"), filtroDTO.getUsuarioAsignadoId()));
            }

            if (filtroDTO.getOrigenId() != null) {
                // origen
                Join<Consulta, Origen> origenJoin = root.join("origen");
                predicates.add(criteriaBuilder.equal(origenJoin.get("id"), filtroDTO.getOrigenId()));
            }

            if (filtroDTO.getEstadoId() != null) {
                // origen
                Join<Consulta, Estado> estadoJoin = root.join("estado");
                predicates.add(criteriaBuilder.equal(estadoJoin.get("id"), filtroDTO.getEstadoId()));
            }

            if (filtroDTO.getCodigo() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codigo"), filtroDTO.getCodigo()));
            }
            if (filtroDTO.getPregunta() != null) {
                predicates.add(criteriaBuilder.like(root.get("pregunta"), "%" + filtroDTO.getPregunta() + "%"));
            }
            if (filtroDTO.getRespuesta() != null) {
                predicates.add(criteriaBuilder.like(root.get("respuesta"), "%" + filtroDTO.getRespuesta() + "%"));
            }

            // cuando la fecha es la misma que la almacenada en bd, no entrega resultados, lo cual es incorrecto
            if (filtroDTO.getFechaCreacionDesde() != null && filtroDTO.getFechaCreacionHasta() != null) {
                predicates.add(criteriaBuilder.between(
                        root.get("fechaCreacion"),
                        filtroDTO.getFechaCreacionDesde(),
                        filtroDTO.getFechaCreacionHasta()
                ));
            } else if (filtroDTO.getFechaCreacionHasta() == null && filtroDTO.getFechaCreacionDesde() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                        root.get("fechaCreacion"), filtroDTO.getFechaCreacionDesde()
                ));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };


        Page<Consulta> paginaRaw = consultaRepository.findAll(parametrosBusqueda, pageable);
        List<ConsultaDTO> consultasDTO = new ArrayList<>();

        ConsultaDTO plantillaConsulta;

        for (Consulta consulta : paginaRaw.getContent()) {
            Usuario creador = consulta.getUsuarioCreador();
            Usuario asignado = consulta.getUsuarioCreador();
            Origen origen = consulta.getOrigen();
            Estado estado = consulta.getEstado();


            plantillaConsulta = ConsultaDTO.builder()
                    .id(consulta.getId())
                    .codigo(consulta.getCodigo())
                    .pregunta(consulta.getPregunta())
                    .respuesta(consulta.getRespuesta())
                    .fechaCreacion(consulta.getFechaCreacion())
                    .fechaActualizacion(consulta.getFechaActualizacion())

                    .usuarioSolicitante((creador == null) ? null : UsuarioDTO.builder().id(creador.getId()).nombres(creador.getPersona().getNombres()).build())
                    .usuarioAsignado((asignado == null) ? null : UsuarioDTO.builder().id(asignado.getId()).nombres(asignado.getPersona().getNombres()).build())

                    .origenId((origen == null) ? null : origen.getId())
                    .origenNombre((origen == null) ? null : origen.getDescripcion().name())

                    .estadoId((estado == null) ? null : estado.getId())
                    .estadoNombre((estado == null) ? null : estado.getDescripcion().name())
                    .build();

            consultasDTO.add(plantillaConsulta);
        }

        return Pagina.builder()
                .data(consultasDTO)
                .currentPage((page == 0) ? 1 : page)
                .totalItems(paginaRaw.getTotalElements())
                .totalPages(paginaRaw.getTotalPages())
                .build();

    }

    // FALTA CREAR EL ENTITY, MAPPER Y REPOSITORY
    @Override
    public List<GenericoDTO> getAllEstados() {
        List<Estado> estados = estadoRepository.findAll();
        return EstadoMapper.INSTANCE.map(estados);

    }

    @Override
    public List<GenericoDTO> getAllOrigen() {
        List<Origen> origenes = origenRepository.findAll();
        return OrigenMapper.INSTANCE.map(origenes);

    }

    @Override
    public List<GenericoDTO> getAllTiposSexo() {
        List<Sexo> sexos = sexoRepository.findAll();
        return SexoMapper.INSTANCE.map(sexos);

    }

    @Transactional
    @Override
    public TransaccionDTO createUsuario(UsuarioDTO usuarioDTO) throws BackendException {
        List<String> validacion = this.validarUsuario(usuarioDTO, false);

        if (validacion.size() > 0) {
            return TransaccionDTO.builder().validaciones(validacion).success(false).build();
        }
        // HAY QUE INSERTARLO ANTES DE ASOCIARLO A LA PERSONA
        Domicilio domicilio = domicilioRepository.save(Domicilio.builder()
                .direccion(usuarioDTO.getDireccion())
                .comuna(usuarioDTO.getComuna())
                .region(usuarioDTO.getRegion()).build());


        // HAY QUE INSERTARLO ANTES DE ASOCIARLO CON EL USUARIO
        Persona persona = Persona.builder()
                .rut(usuarioDTO.getRut())
                .nombres(usuarioDTO.getNombres())
                .apellidos(usuarioDTO.getApellidos())
                .fechaNacimiento(new Date())
                .domicilio(domicilio).build();

        if (usuarioDTO.getSexoId() != null) {
            persona.setSexo(sexoRepository.findById(usuarioDTO.getSexoId()).orElse(null));
        }


        // se debe llenar el grupo de forma obligatoria
        // si no viene dentro de los campos el id del grupo se asume que es un ciudadano grupo=3
        // si devuelve nulo quiere decir que fallo la conexion hacia la bd o algo similar
        persona.setGrupo(grupoRepository.findById((usuarioDTO.getGrupoId() != null) ? usuarioDTO.getGrupoId() : 3).orElse(Grupo.builder().id(3).build()));

        personaRepository.save(persona);

        Usuario usuario = Usuario.builder()
                .activo(true)
                .blocked(false)
                .admin(false)
                .password(encoder.encode(usuarioDTO.getPassword()))
                .username(usuarioDTO.getEmail())
                .persona(persona).build();

        usuario.setRol(rolRepository.findById((usuarioDTO.getRolId() != null) ? usuarioDTO.getRolId() : 1).orElse(Rol.builder().id(1).build()));

        usuarioRepository.save(usuario);

        return TransaccionDTO.builder().msg("OK").success(true).singleObject(singletonList(usuarioDTO)).build();

    }

    @Transactional
    @Override
    public TransaccionDTO updateUsuario(UsuarioDTO usuarioDTO) throws BackendException {
        List<String> validacion = this.validarUsuario(usuarioDTO, true);

        if (validacion.size() > 0) {
            return TransaccionDTO.builder().validaciones(validacion).success(false).build();
        }

        Usuario usuario = usuarioRepository.findById(usuarioDTO.getId()).orElse(null);

        if (usuario != null) {
            Persona persona = (usuario.getPersona() == null) ? new Persona() : usuario.getPersona();

            // llenamos datos de la persona
            persona.setRut((usuarioDTO.getRut() == null) ? persona.getRut() : usuarioDTO.getRut());
            persona.setNombres((usuarioDTO.getRut() == null) ? persona.getNombres() : usuarioDTO.getNombres());
            persona.setFechaNacimiento((usuarioDTO.getFechaNacimiento() == null) ? persona.getFechaNacimiento() : usuarioDTO.getFechaNacimiento());
            persona.setApellidos((usuarioDTO.getApellidos() == null) ? persona.getApellidos() : usuarioDTO.getApellidos());
            persona.setSexo((usuarioDTO.getSexoId() == null) ? persona.getSexo() : Sexo.builder().id(usuarioDTO.getSexoId()).build());

            // llenamos datos del domicilio
            Domicilio domicilio = (persona.getDomicilio() == null) ? new Domicilio() : persona.getDomicilio();
            domicilio.setComuna(usuarioDTO.getComuna());
            domicilio.setRegion(usuarioDTO.getRegion());
            domicilio.setDireccion(usuarioDTO.getDireccion());

            // si no existe registro en domicilio lo creamos
            if (domicilio.getId() == null) {
                domicilioRepository.save(domicilio);
            }

            persona.setDomicilio(domicilio);

            // si no existe registro en personas lo creamos
            if (persona.getId() != null) {
                personaRepository.save(persona);
            }

            // llenamos datos de la autentificacion del usuario
            usuario.setUsername((usuarioDTO.getEmail() == null) ? usuario.getUsername() : usuarioDTO.getEmail());
            usuario.setBlocked((usuarioDTO.getBlocked() == null) ? usuario.getBlocked() : usuarioDTO.getBlocked());
            usuario.setActivo((usuarioDTO.getActivo() == null) ? usuario.isActivo() : usuarioDTO.getActivo());
            usuario.setPersona(persona);

            usuarioRepository.save(usuario);

            return TransaccionDTO.builder().msg("OK").success(true).build();
        }

        return TransaccionDTO.builder().msg("NOK").success(false).msg("Usuario no encontrado").build();
    }

}

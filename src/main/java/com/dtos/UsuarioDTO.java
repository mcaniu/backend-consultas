package com.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioDTO {

    private Integer id;

    @Email(message = "Debe proporcionar un correo válido.")
    @NotBlank(message = "El correo no debe estar vacio.")
    private String email;

    @Size(min = 6, message = "La contraseña debe tener al menos 6 caracteres.")
    @NotBlank(message = "La contraseña no debe estar vacia.")
    private String password;
    private Boolean blocked;
    private Boolean admin;
    private Boolean activo;
    private Integer rolId;

    @NotBlank(message = "El rut no debe estar vacio.")
    private String rut;

    @NotBlank(message = "El nombre no debe estar vacio.")
    private String nombres;

    @NotBlank(message = "El apelllido no debe estar vacio.")
    private String apellidos;


    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date fechaNacimiento;

    private Integer sexoId;
    private Integer grupoId;

    @NotBlank(message = "La dirección no debe estar vacio.")
    private String direccion;

    @NotBlank(message = "La región no debe estar vacio.")
    private String region;

    @NotBlank(message = "La comuna no debe estar vacio.")
    private String comuna;

    private String telefono;
    private String celular;

}

package com.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FiltroDTO {

    private String codigo;
    private String pregunta;
    private String respuesta;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date fechaCreacionDesde;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date fechaCreacionHasta;

    private Integer usuarioCreadorId;
    private Integer usuarioAsignadoId;
    private Integer origenId;
    private Integer estadoId;
    private String rutSolicitante;

}

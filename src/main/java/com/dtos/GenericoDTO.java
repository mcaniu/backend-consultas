package com.dtos;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenericoDTO {

    private Integer id;
    private String descripcion;
    private Boolean activo;

}

package com.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonaDTO {

    private Integer usuarioId;
    private String nombres;
    private String rut;
    private String apellidos;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date fechaNacimiento;

    private String direccion;
    private String comuna;
    private String region;
    private Integer sexoId;
    private Integer grupoId;

    private String telefono;
    private String email;
    private String celular;
    // falta el email
    // falta el telefono
    // falta el celular


}

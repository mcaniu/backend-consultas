package com.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * DTO que transportaran los datos de acceso del usuario.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginDTO {
    @NotBlank(message = "username no debe estar vacio")
    private String username;

    @NotBlank(message = "password no debe estar vacia")
    private String password;

}

package com.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pagina {

    private Object data;
    private int currentPage;
    private long totalItems;
    private int totalPages;

}

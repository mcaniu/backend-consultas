package com.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConsultaDTO {

    private Integer id;
    private String codigo;
    private String pregunta;
    private String respuesta;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date fechaCreacion;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date fechaActualizacion;

    private UsuarioDTO usuarioSolicitante;
    private UsuarioDTO usuarioAsignado;
    private Integer usuarioAsignadoId;
    private String usuarioAsignadoNombre;
    private Integer origenId;
    private String origenNombre;
    private Integer estadoId;
    private String estadoNombre;

}

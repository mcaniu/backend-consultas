package com.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * DTO que transportaran los datos de token del usuario.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtDTO {

    private String token;
    private final String type = "Bearer";
    private Integer id;
    private String username;
    private String email;
    private List<String> roles;

}

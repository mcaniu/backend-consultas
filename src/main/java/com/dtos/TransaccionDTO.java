package com.dtos;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransaccionDTO {

    private String msg;
    private Boolean success;
    private List<Object> listObject;
    private List<String> validaciones;
    private Object singleObject;

}

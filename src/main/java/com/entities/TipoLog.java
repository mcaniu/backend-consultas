package com.entities;

import com.enums.ETipoLog;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "tipo_log")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private ETipoLog descripcion;

    @Column(name = "activo")
    private boolean activo;


}
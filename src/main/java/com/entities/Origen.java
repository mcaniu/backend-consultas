package com.entities;

import com.enums.EOrigen;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "origen")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Origen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EOrigen descripcion;

    @Column(name = "activo")
    private boolean activo;


}
package com.entities;

import com.enums.ERol;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "rol")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Rol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private ERol descripcion;

    @Column(name = "activo")
    private boolean activo;


}
package com.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


/**
 * Entity que almacena los datos de rol de usuario.
 */
@Entity
@Table(name = "rol")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogConsulta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "datos")
    private String datos;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consulta_id", referencedColumnName = "id")
    private Consulta consulta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_actual_id", referencedColumnName = "id")
    private Usuario usuarioActual;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_cambio_id", referencedColumnName = "id")
    private Usuario usuarioCambio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_log_id", referencedColumnName = "id")
    private TipoLog tipoLog;


}
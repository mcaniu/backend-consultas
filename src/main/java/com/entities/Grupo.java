package com.entities;

import com.enums.EGrupo;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "grupo")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Grupo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EGrupo descripcion;

    @Column(name = "activo")
    private boolean activo;

}
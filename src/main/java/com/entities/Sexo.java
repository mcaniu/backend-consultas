package com.entities;

import com.enums.ESexo;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "sexo")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Sexo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private ESexo descripcion;

}
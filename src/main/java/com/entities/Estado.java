package com.entities;

import com.enums.EEstado;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "estado")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Estado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EEstado descripcion;

    @Column(name = "activo")
    private boolean activo;


}
package com.repository;

import com.entities.Sexo;
import com.enums.ESexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SexoRepository extends JpaRepository<Sexo, Integer> {

    Optional<Sexo> findByDescripcion(ESexo name);

}

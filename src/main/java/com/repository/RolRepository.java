package com.repository;

import com.entities.Rol;
import com.enums.ERol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {

    Optional<Rol> findByDescripcion(ERol name);

}

package com.repository;

import com.entities.TipoLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoLogRepository extends JpaRepository<TipoLog, Integer> {
}

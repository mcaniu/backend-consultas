package com.repository;

import com.entities.Origen;
import com.enums.EOrigen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrigenRepository extends JpaRepository<Origen, Integer> {

    Optional<Origen> findByDescripcion(EOrigen descripcion);
}

package com.repository;

import com.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<Usuario, Integer> {

    Optional<Usuario> findByUsername(String usrUsername);

    Boolean existsByUsername(String usrUsername);

    Boolean existsByUsernameAndId(String usrUsername, Integer id);

}

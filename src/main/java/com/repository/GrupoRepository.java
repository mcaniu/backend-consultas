package com.repository;

import com.entities.Grupo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Integer> {

    @Query(value = "SELECT  count (*) FROM  usuario u JOIN persona p  ON p.id=u.persona_id JOIN grupo g ON g.id = p.grupo_id WHERE u.id = :usuarioId AND g.id IN(4,5,6)",
            nativeQuery = true)
    int esGrupoValido(@Param("usuarioId") Integer usuarioId);

}

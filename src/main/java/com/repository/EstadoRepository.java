package com.repository;

import com.entities.Estado;
import com.enums.EEstado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {

    Optional<Estado> findByDescripcion(EEstado descripcion);
}

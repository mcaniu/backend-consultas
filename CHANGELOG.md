## [1.0.0] - 2024-02-28
### Modificado
- se crea carpeta con documentacion con ejemplos de la fuente original
- se cambia datasource hacia bd h2
- se agrega acceso conexion con h2 via codigo 
- se elimina referencia hacia otros datasources
- creacion de roles via clase CommandLineRunner
- creacion automatica de base de datos
- pasar de maven a gradle
- eliminar git de fuente original y traspasar a repositorio propio
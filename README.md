# template spring security

## Proyecto plantilla de autentificacion spring security
Proyecto backend que emula a un sistema de atencion ciudadana donde se atienden consultas y funcionarios 
dan solucion a dichas consultas.


## Futuras mejoras
- cuando la fecha es la misma que la almacenada en bd, no entrega resultados, lo cual es incorrecto
- crear un metodo que permita verificar la carga de consultas de los funcionarios y realizar asignacion
(se debe recrear varios usuarios y consultas tanto finalizadas como ingresadas para la creacion de la funcion)

- envio de correo asignacion de usuario funcionario y creacion de plantilla
- envio de correo derivacion de usuario funcionario y creacion de plantilla
- envio de correo creacion consulta usuario ciudadano y plantilla
- envio de correo resolucion consulta usuario ciudadano y plantilla
- adicionar archivos para las consultas(MAS ADELANTE)
- crear un usuario sistema para que el front inicie sesion en el y que el backend realice insercion o actualizacion 
de un usuario ciudadano sin necesidad de conectarse el

- cuando es usuario sistema, enviar parametros adicionales opcionales para insercion o actualizacion de nuevo usuario
- controlar las execpciones mas obvias y retornar mensajes en transaccion dto
- aplicar valores por defecto en tabla personas
- verificar como no enviar el campo trace al lanzar errores de validaciones
- modificacion url clase de seguridad
- adicion de swagger
- pruebas unitarias
- documentacion con javadoc
## Tecnologias aplicadas
- java 1.8
- springboot v2.7.2
- gradle-6.8
- jsonwebtoken jjwt 0.9.1(old)

